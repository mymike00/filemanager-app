��    b      ,  �   <      H     I     O     _     x  	   �     �  +   �  1   �     �     	     $	     6	     =	     ]	     t	     �	     �	     �	     �	     �	     �	     �	      
  (   '
  "   P
     s
  $   �
     �
  $   �
     �
      �
          *     .     3     :  (   I     r     �  
   �     �     �     �     �  #   �               +     :      O     p     �     �     �     �  	   �     �     �     �     �     �     �  	   �     �     �               $     1     6  
   =     H     W     `     g     t     �     �     �     �     �     �     �  
   �     �     �          $     )     1     :     J     [  ,   a     �  +   �  +   �  M       S     [  %   n     �     �     �  =   �  ;        B     \  %   x     �  >   �  ,   �  #   $     H     `  )   v     �     �  #   �      �  :     L   B  *   �     �  .   �  !   	  :   +  $   f  /   �     �     �  
   �     �     �  4   �     1  5   N  
   �     �     �  $   �  (   �  .        H     W     o  !   �  .   �  .   �               %  
   4     ?     T     [  
   k  
   v     �  ,   �     �  !   �      �          "     8  
   D  
   O     Z     i  
   �     �      �     �     �  
      (     
   4     ?  &   L     s     �  %   �  )   �     �     �  	               "   1     T  a   c  1   �  <   �  ^   4         b                    %      3   -   =   !   "   N           I       >   a                    Q      <   V       R   W   ?                 Z   J   L   K       2   /       +   ,              ^             8   B         ;      F   :   @                4              E              `      *   H      [   \   5   Y       #   0   A   U       S             T   7                          (       6   
   M   ]       '          9   )   G   X   	   O   $      &   .       1   C   P   _   D         Copy  does not exist  it needs Authentication %1 Files Accessed: Archive file Are you sure you want to extract '%1' here? Are you sure you want to permanently delete '%1'? Authentication failed Authentication required Can not access %1 Cancel Cannot access File or Directory Cannot copy/move items Cannot move items Choose action Clear clipboard Close this tab Copy Could not create file Could not create link to Could not create the directory Could not create trash info file Could not find a suitable name to backup Could not move the directory/file  Could not open file Could not remove the directory/file  Could not remove the item  Could not remove the trash info file Could not rename Could not set permissions to dir Create file Cut Date Delete Deleting files Do you want to extract the archive here? Enter a new name Error creating new folder Executable Extract Archive Extract archive Extracting archive '%1' Extracting failed Extracting the archive '%1' failed. File %1 File Manager File operation File operation error File or Directory does not exist Folder not accessible For file: %1 Go To Icons List Modified: Name No files OK Ok Open Open in a new tab Open with Open with another app Operation in progress Password Paste files Permissions: Pick Places Properties Read error in  Readable Rename Rename error Save password Saved to: %1 Select Set permissions error in  Settings Share Show Hidden Files Sort By Sort Order There is no space to copy There is no space to download Unknown User View As Writable Write error in  cannot write in  items net tool not found, check samba installation no write permission on folder  origin and destination folders are the same path or url may not exist or cannot be read Project-Id-Version: ubuntu-filemanager-app
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-03 13:35+0000
PO-Revision-Date: 2018-12-21 10:49+0000
Last-Translator: Hussain Hashem Aljafri <baddea14@gmail.com>
Language-Team: Arabic <https://translate.ubports.com/projects/ubports/filemanager-app/ar/>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Weblate 3.1.1
X-Launchpad-Export-Date: 2017-04-08 06:04+0000
  نسخ  غير موجود  فإنه يحتاج المصادقة ٪ 1 ملفات الوصول: ملف الأرشيف هل تريد بالتأكيد استخراج "٪ 1" هنا؟ هل تريد بالتأكيد حذف '٪ 1' نهائيا؟ المصادقة فشلت يستلزم التوثيق لا يمكن الوصول إلى٪ 1 إلغاء;الإلغاء لا يمكن الوصول إلى الملف أو الدليل لا يمكن نسخ / نقل العناصر لا يمكن نقل العناصر اختر الإجراء مسح الحافظة أغلق علامة التبويب هذه نسخ تعذر إنشاء ملف تعذر إنشاء رابط إلى تعذر إنشاء الدليل تعذر إنشاء ملف معلومات المهملات تعذر العثور على اسم مناسب للنسخ الاحتياطي تعذر نقل الدليل / الملف  لا يمكن فتح الملف تعذر إزالة الدليل / الملف  تعذر إزالة العنصر  تعذر إزالة ملف معلومات المهملات تعذرت إعادة التسمية تعذر تعيين الأذونات إلى dir إنشاء ملف قطع تاريخ حذف حذف الملفات هل تريد استخراج الأرشيف هنا؟ أدخل اسما جديدا حدث خطأ أثناء إنشاء مجلد جديد تنفيذ استخراج الأرشيف استخراج الأرشيف استخراج الأرشيف '٪ 1' أخفقت عملية الاستخراج أخفق استخراج الأرشيف '٪ 1'. الملف٪ 1 مدير الملفات تشغيل الملف خطأ في تشغيل الملف الملف أو الدليل غير موجود لا يمكن الوصول إلى المجلد للملف:٪ 1 اذهب إلى أيقونات قائمة تم التعديل: اسم لا ملفات موافق موافق افتح فتح في علامة تبويب جديدة مفتوحة مع فتح مع التطبيق آخر عملية قيد التنفيذ كلمة المرور لصق الملفات ضوابط: إلتقط أماكن الخصائص قراءة الخطأ في  مقروء إعادة تسمية إعادة تسمية الخطأ حفظ كلمة المرور تم الحفظ إلى:٪ 1 تحديد تعيين خطأ الأذونات في  الضبط مشاركة أظهر الملفات المخفية ترتيب ك امر ترتيب ليس هناك مساحة للنسخ ليس هناك مساحة للتحميل غير معروف مستخدم عرض ك للكتابة اكتب خطأ في  لا يمكن الكتابة في  العناصر صافي أداة لم يتم العثور عليها، والتحقق من تركيب سامبا لا إذن الكتابة على المجلد _  المقصد و المجلدات الأصل  هي نفسها مسار أو عنوان url قد لا تكون موجودة أو لا يمكن قراءتها 