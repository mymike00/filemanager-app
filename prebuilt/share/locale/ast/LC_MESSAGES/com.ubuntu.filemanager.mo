��    ^           �      �     �     �          (  	   1     ;  +   H  1   t     �     �     �     �     �     	     $	     6	     D	     T	     c	     h	     ~	     �	      �	  (   �	  "    
     #
  $   7
     \
  $   w
     �
      �
     �
     �
     �
     �
     �
  (   �
     "     3  
   M     X     h     x     �  #   �     �     �     �     �      �           6     C     I     O  	   T     ^     c     l     o     r     w  	   �     �     �     �     �     �     �  
   �     �                         -     4     N     W     i  
   q     |     �     �     �     �     �     �     �     �     �  +     +   D  �  p     i     q     }     �  	   �     �  -   �  (   �          .     H  	   ^  (   h     �     �     �     �     �               ,     E  4   b  >   �  %   �     �  *         A  <   b     �  0   �     �     �     �  
   �       !        ?     V  
   u     �     �     �     �  '   �  
   �               /  !   N     p     �     �     �     �  	   �     �     �     �     �     �     �  	   �          "     9     E  	   U     _  
   h     s     �     �     �     �     �      �     �     �               )     C     ]     i     q  
   z     �     �  	   �  '   �  -   �  %        M         6   T       H   -         	   Q   N   J   K   4                2   )      D   /   8   ^   '   C              >          L          5          R   %   (          \                     ,                     @       X   V              Z          <                            #       ]       1   :   9   $      0   O         "                      7   ;   .       W   =       U   I   3   
   G   F   +   S   Y              !   &       [   P   B       *      A   ?   E     Copy  does not exist  it needs Authentication %1 Files Accessed: Archive file Are you sure you want to extract '%1' here? Are you sure you want to permanently delete '%1'? Authentication failed Authentication required Can not access %1 Cancel Cannot access File or Directory Cannot copy/move items Cannot move items Choose action Clear clipboard Close this tab Copy Could not create file Could not create link to Could not create the directory Could not create trash info file Could not find a suitable name to backup Could not move the directory/file  Could not open file Could not remove the directory/file  Could not remove the item  Could not remove the trash info file Could not rename Could not set permissions to dir Create file Cut Date Delete Deleting files Do you want to extract the archive here? Enter a new name Error creating new folder Executable Extract Archive Extract archive Extracting archive '%1' Extracting failed Extracting the archive '%1' failed. File %1 File Manager File operation File operation error File or Directory does not exist Folder not accessible For file: %1 Go To Icons List Modified: Name No files OK Ok Open Open in a new tab Open with Open with another app Operation in progress Password Paste files Permissions: Places Properties Read error in  Readable Rename Rename error Save password Select Set permissions error in  Settings Show Hidden Files Sort By Sort Order There is no space to copy There is no space to download Unknown User View As Writable Write error in  cannot write in  items no write permission on folder  origin and destination folders are the same path or url may not exist or cannot be read Project-Id-Version: ubuntu-filemanager-app
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-03 13:35+0000
PO-Revision-Date: 2018-01-02 22:46+0000
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian <https://translate.ubports.com/projects/ubports/filemanager-app/ast/>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.15
X-Launchpad-Export-Date: 2017-04-08 06:04+0000
  Copiar  nun esiste  precisa l'autenticación %1 ficheros Accedíu: Ficheru d'archivu ¿De xuru que quies estrayer el ficheru '%1'? ¿De xuru quies desaniciar dafechu '%1'? Falló l'autenticación Ríquese l'autenticación Nun pue accedese a %1 Encaboxar Nun pue accedese al ficheru o direutoriu Nun pue copiar/mover elementos Nun puen movese los elementos Escueyi l'aición Llimpiar cartafueyu Zarrar esta llingüeta Copiar Nun pudo crease'l ficheru Nun pudo crease l'enllaz Nun pudo crease'l direutoriu Nun pudo crease'l ficheru d'información de papelera Nun pudo alcontrase un nome afayadizu pa la copia de seguranza Nun pudo movese'l direutoriu/ficheru  Nun pudo abrise'l ficheru Nun pudo desaniciase'l direutoriu/ficheru  Nun pudo desaniciase l'elementu  Nun pudo desaniciase'l ficheru d'información de la papelera Nun pudo renomase Nun pudieron afitase los permisos del direutoriu Crear ficheru Cortar Data Desaniciar Desaniciando ficheros ¿Quies estrayer l'archivu equí? Introduz un nome nuevu Fallu creando la carpeta nueva Executable Estrayer archivu Estrayer archivu Estrayendo'l ficheru '%1' Falló la estraición Falló la estraición del archivu '%1'. Ficheru %1 Xestor de ficheros Operación de ficheros Fallu na operación de ficheru Nun esiste'l ficheru o direutoriu Carpeta non accesible Pal ficheru: %1 Dir a Iconos Llista Camudáu: Nome Nun hai ficheros Aceutar Aceutar Abrir Abrir nuna llingüeta nueva Abrir con Abrir con otra aplicacioón Operación en progresu Contraseña Apegar ficheros Permisos: Llugares Propiedaes Fallu de llectura en  Lleíble Renomar Fallu al renomar Guardar contraseña Esbillar Fallu al afitar los permisos en  Axustes Amosar ficheros anubríos Ordenar por Mou d'ordenación Nun hai espaciu pa copiar Nun hai espaciu pa  baxar Desconocíu Usuariu Ver como Escribible Fallu d'escritura en  nun pue escribise en  elementos nun hai permisu d'escritura na carpeta  les carpetes d'orixe y destín son les mesmes podríen nun lleese'l camín o la url 